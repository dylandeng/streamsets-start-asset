# Inject



Identifies injectable constructors, methods, and fields. May apply to static as well as instance members. An injectable member may have any access modifier (private, package-private, protected, public). Constructors are injected first, followed by fields, and then methods. Fields and methods in superclasses are injected before those in subclasses. Ordering of injection among fields and among methods in the same class is not specified.

标识可注入构造函数、方法和字段。可以应用于静态成员，也可以应用于实例成员。可注入成员可以有任何访问修饰符(private、package-private、protected、public)。首先注入构造函数，然后是字段，最后是方法。超类中的字段和方法在子类中的字段和方法之前注入。未指定同一类中的字段和方法之间的注入顺序。



Injectable constructors are annotated with @Inject and accept zero or more dependencies as arguments. @Inject can apply to at most one constructor per class.

可注入构造函数使用@Inject进行注释，并接受零或多个依赖项作为参数。@Inject最多可以应用于一个类的一个构造函数。



@Inject 

ConstructorModifiers SimpleTypeName(FormalParameterListopt) Throws ConstructorBody
@Inject is optional for public, no-argument constructors when no other constructors are present. This enables injectors to invoke default constructors.

@Inject对于没有其他构造函数的公共无参数构造函数是可选的。这使得注入器能够调用默认构造函数。

@Injectopt Annotationsopt public SimpleTypeName() Throwsopt ConstructorBody



Injectable fields:
are annotated with @Inject.
are not final.
may have any otherwise valid name.

@Inject FieldModifiersopt Type VariableDeclarators;
Injectable methods:
are annotated with @Inject.
are not abstract.
do not declare type parameters of their own.
may return a result
may have any otherwise valid name.
accept zero or more dependencies as arguments.

@Inject MethodModifiersopt ResultType Identifier(FormalParameterListopt) Throwsopt MethodBody
The injector ignores the result of an injected method, but non-void return types are allowed to support use of the method in other contexts (builder-style method chaining, for example).
Examples:
   public class Car {
     // Injectable constructor
     @Inject public Car(Engine engine) { ... }

     // Injectable field
     @Inject private Provider<Seat> seatProvider;
    
     // Injectable package-private method
     @Inject void install(Windshield windshield, Trunk trunk) { ... }
   }
A method annotated with @Inject that overrides another method annotated with @Inject will only be injected once per injection request per instance. A method with no @Inject annotation that overrides a method annotated with @Inject will not be injected.
Injection of members annotated with @Inject is required. While an injectable member may use any accessibility modifier (including private), platform or injector limitations (like security restrictions or lack of reflection support) might preclude injection of non-public members.
Qualifiers
A qualifier may annotate an injectable field or parameter and, combined with the type, identify the implementation to inject. Qualifiers are optional, and when used with @Inject in injector-independent classes, no more than one qualifier should annotate a single field or parameter. The qualifiers are bold in the following example:
   public class Car {
     @Inject private @Leather Provider<Seat> seatProvider;

     @Inject void install(@Tinted Windshield windshield,
         @Big Trunk trunk) { ... }
   }
If one injectable method overrides another, the overriding method's parameters do not automatically inherit qualifiers from the overridden method's parameters.
Injectable Values
For a given type T and optional qualifier, an injector must be able to inject a user-specified class that:
is assignment compatible with T and
has an injectable constructor.
For example, the user might use external configuration to pick an implementation of T. Beyond that, which values are injected depend upon the injector implementation and its configuration.
Circular Dependencies
Detecting and resolving circular dependencies is left as an exercise for the injector implementation. Circular dependencies between two constructors is an obvious problem, but you can also have a circular dependency between injectable fields or methods:
   class A {
     @Inject B b;
   }
   class B {
     @Inject A a;
   }
When constructing an instance of A, a naive injector implementation might go into an infinite loop constructing an instance of B to set on A, a second instance of A to set on B, a second instance of B to set on the second instance of A, and so on.
A conservative injector might detect the circular dependency at build time and generate an error, at which point the programmer could break the circular dependency by injecting Provider<A> or Provider<B> instead of A or B respectively. Calling get() on the provider directly from the constructor or method it was injected into defeats the provider's ability to break up circular dependencies. In the case of method or field injection, scoping one of the dependencies (using singleton scope, for example) may also enable a valid circular relationship.
@Target(value={METHOD,CONSTRUCTOR,FIELD})
@Retention(value=RUNTIME)
@Documentedpublic @interface Inject



See Also:
@Qualifier, Provider

